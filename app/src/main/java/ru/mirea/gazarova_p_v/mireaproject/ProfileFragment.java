package ru.mirea.gazarova_p_v.mireaproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ru.mirea.gazarova_p_v.mireaproject.databinding.FragmentProfileBinding;

public class ProfileFragment extends Fragment {

    FragmentProfileBinding binding;
    EditText editText1;
    EditText editText2;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = FragmentProfileBinding.inflate(getLayoutInflater());
        editText1 = binding.editText1;
        editText2 = binding.editText2;
        button = binding.button;

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("CHECK", "aaa");
                SharedPreferences sharedPref = getActivity().getSharedPreferences("profile_settings", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("NAME", String.valueOf(editText1.getText()));
                editor.putString("SURNAME", String.valueOf(editText2.getText()));
                editor.apply();

                Toast.makeText(getActivity(), "Сохраненные данные: " + sharedPref.getString("NAME", "") + " " + sharedPref.getString("SURNAME", ""), Toast.LENGTH_SHORT).show();
            }
        });


        return binding.getRoot();
    }
}