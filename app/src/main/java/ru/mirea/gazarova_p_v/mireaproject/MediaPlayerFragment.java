package ru.mirea.gazarova_p_v.mireaproject;

import static android.Manifest.permission.FOREGROUND_SERVICE;
import static android.Manifest.permission.POST_NOTIFICATIONS;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import ru.mirea.gazarova_p_v.mireaproject.databinding.FragmentMediaPlayerBinding;

public class MediaPlayerFragment extends Fragment {

    private int PermissionCode = 200;
    private FragmentMediaPlayerBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final int[] isMusicPlay = {0};

        binding = FragmentMediaPlayerBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        if (ContextCompat.checkSelfPermission(getContext(), POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {

            Log.d(MainActivity.class.getSimpleName().toString(), "Разрешения получены");
        } else {
            Log.d(MainActivity.class.getSimpleName().toString(), "Нет разрешений!");

            ActivityCompat.requestPermissions(getActivity(), new String[]{POST_NOTIFICATIONS, FOREGROUND_SERVICE}, PermissionCode);

        }
        binding.playPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isMusicPlay[0] == 0)
                {
                    isMusicPlay[0] = 1;
                    Intent serviceIntent = new Intent(getActivity(), MusicPlayerService.class);
                    getActivity().startForegroundService(serviceIntent);
                    binding.playPauseBtn.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.baseline_pause_circle_24, null));
                }
                else
                {
                    isMusicPlay[0] = 0;
                    getActivity().stopService(
                            new Intent(getActivity(), MusicPlayerService.class));
                    binding.playPauseBtn.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.baseline_play_circle_24, null));
                }

            }
        });

        return root;
    }
}
