package ru.mirea.gazarova_p_v.mireaproject;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.mirea.gazarova_p_v.mireaproject.databinding.FragmentSensorBinding;

public class SensorFragment extends Fragment implements SensorEventListener {

    private FragmentSensorBinding binding;
    private TextView textViewTemperature;
    private SensorManager sensorManager;
    private Sensor tempSensor;
    private  Boolean isTemperatureSensorAvailable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSensorBinding.inflate(inflater, container, false);

        textViewTemperature = binding.textViewTemperature;
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null) {

            tempSensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
            Log.d("Sensor", "Sensor found");
            isTemperatureSensorAvailable = true;
        }
        else {
            textViewTemperature.setText("Temperature sensor is not available");
            Log.d("Sensor", "Sensor not found");
            isTemperatureSensorAvailable = false;
        }


        return binding.getRoot();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        textViewTemperature.setText(event.values[0]+" °C");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public  void onResume() {
        super.onResume();
        if (isTemperatureSensorAvailable) {
            sensorManager.registerListener(this, tempSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isTemperatureSensorAvailable) {
            sensorManager.unregisterListener(this);
        }
    }
}