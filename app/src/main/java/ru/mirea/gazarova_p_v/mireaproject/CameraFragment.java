package ru.mirea.gazarova_p_v.mireaproject;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.mirea.gazarova_p_v.mireaproject.databinding.FragmentCameraBinding;

public class CameraFragment extends Fragment {

    private	static	final	int	REQUEST_CODE_PERMISSION	=	100;
    private	boolean	isWork	=	false;
    private Uri imageUri;
    FragmentCameraBinding binding;
    private ImageView imageViewCamera1;
    private ImageView imageViewCamera2;
    private Button buttonCamera;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = FragmentCameraBinding.inflate(getLayoutInflater());
        imageViewCamera1 = binding.imageViewCamera1;
        imageViewCamera2 = binding.imageViewCamera2;
        buttonCamera = binding.buttonCamera;

        imageViewCamera1.setColorFilter(Color.BLUE, PorterDuff.Mode.LIGHTEN);
        imageViewCamera2.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);

        int cameraPermissionStatus = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA);
        if (cameraPermissionStatus == PackageManager.PERMISSION_GRANTED)  {
            isWork = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),	new	String[]	{android.Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},	REQUEST_CODE_PERMISSION);
        }

        ActivityResultCallback<ActivityResult> callback	=	new	ActivityResultCallback<ActivityResult>()	{
            @Override
            public	void	onActivityResult(ActivityResult	result)	{
                if	(result.getResultCode()	==	Activity.RESULT_OK)	{
                    Intent data	=result.getData();
                    imageViewCamera1.setImageURI(imageUri);
                    imageViewCamera2.setImageURI(imageUri);
                }
            }
        };

        ActivityResultLauncher<Intent> cameraActivityResultLauncher	= registerForActivityResult(
                new	ActivityResultContracts.StartActivityForResult(),
                callback);

        buttonCamera.setOnClickListener(new	View.OnClickListener()	{
            @Override
            public	void	onClick(View	v)	{
                Intent	cameraIntent	=	new	Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Log.d("iswork", String.valueOf(isWork));

                if	(isWork)	{
                    try	{
                        File photoFile	=	createImageFile();
                        String	authorities	=	getActivity().getPackageName()	+	".fileprovider";
                        imageUri	=	FileProvider.getUriForFile(getActivity(),	authorities,	photoFile);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,	imageUri);
                        cameraActivityResultLauncher.launch(cameraIntent);
                    }	catch	(IOException e)	{
                        e.printStackTrace();
                    }
                }
            }
        });

        return binding.getRoot();
    }

    private	File	createImageFile()	throws	IOException	{
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return	File.createTempFile(imageFileName,	".jpg",	storageDirectory);
    }

    @Override
    public	void	onRequestPermissionsResult(int	requestCode, @NonNull String[]	permissions, @NonNull	int[]	grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION:
                isWork = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!isWork) getActivity().finish();
    }
}