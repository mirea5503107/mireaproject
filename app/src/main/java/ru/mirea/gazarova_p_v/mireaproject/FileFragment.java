package ru.mirea.gazarova_p_v.mireaproject;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import ru.mirea.gazarova_p_v.mireaproject.databinding.FragmentFileBinding;

public class FileFragment extends Fragment {

FragmentFileBinding binding;

EditText editText1;
EditText editText2;
Button button1;
Button button2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = FragmentFileBinding.inflate(getLayoutInflater());

        button1 = binding.button1;
        button2 = binding.button2;
        editText1 = binding.editText1;
        editText2 = binding.editText2;

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
                String fileName = String.valueOf(editText1.getText());
                File file = new File(path, fileName + ".txt");
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file.getAbsoluteFile());
                    OutputStreamWriter output = new OutputStreamWriter(fileOutputStream);
                    String data = String.valueOf(editText2.getText());
                    data = cipher(data, 3);
                    output.write(data);
                    output.close();
                    Toast.makeText(getActivity(), "Данные зашифрованы и сохранены", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Log.w("ExternalStorage", "Error writing " + file, e);
                }
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File path = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS);
                String fileName = String.valueOf(editText1.getText());
                File file = new File(path, fileName + ".txt");
                try {
                    FileInputStream fileInputStream = new FileInputStream(file.getAbsoluteFile());

                    InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);

                    List<String> lines = new ArrayList<>();
                    BufferedReader reader = new BufferedReader(inputStreamReader);
                    String line = reader.readLine();
                    while (line != null) {
                        lines.add(line);
                        line = reader.readLine();
                    }
                    String data = decipher(lines.toString(), 3);
                    data = data.substring(1, data.length()-1);

                    editText2.setText(data);
                    Log.w("ExternalStorage", String.format("Read from file %s successful", lines.toString()));
                } catch (Exception e) {
                    Log.w("ExternalStorage", String.format("Read from file %s failed", e.getMessage()));
                }
            }
        });

        return binding.getRoot();
    }

    String cipher(String message, int offset){
        String result = new String();
        for (char character : message.toCharArray()) {
            if (character != ' ') {
                int originalAlphabetPosition = character - 'a';
                int newAlphabetPosition = (originalAlphabetPosition + offset) % 26;
                char newCharacter = (char) ('a' + newAlphabetPosition);
                result += newCharacter;
            } else {
                result += character;
            }
        }
        return result;
    }

    String decipher(String message, int offset) {
        return cipher(message, 26 - (offset % 26));
    }
}